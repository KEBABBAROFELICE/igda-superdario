using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public float VerticalSpeed;
    public float LifetimeInSeconds;

    private Transform m_Transform;
    private void Awake()
    {
        TryGetComponent(out m_Transform);
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Disappear());

        IEnumerator Disappear()
        {
            yield return new WaitForSeconds(LifetimeInSeconds);

            Destroy(gameObject);
        }
    }

    private void Update()
    {
        m_Transform.position = m_Transform.position + Vector3.up * VerticalSpeed * Time.deltaTime;
    }
}
