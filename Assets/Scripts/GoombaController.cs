using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class GoombaController : MonoBehaviour
{
    public float MovementDirection;
    public float MovementSpeedInUnitsPerSecond;
    public float DeathDelayInSeconds;
    public Vector2 DeathBounceVelocityInUnitsPerSecond;
    public float PlayerBounceSpeedInUnitsPerSecond;
    public LayerMask GroundMask;

    private Rigidbody2D m_Rigidibody;
    private Collider2D m_Collider;
    private bool m_Alive = true;


    private void Awake()
    {
        TryGetComponent(out m_Rigidibody);
        TryGetComponent(out m_Collider);
    }

    private void FixedUpdate()
    {
        if (!m_Alive)
            return;

        RaycastHit2D Hit = Physics2D.Raycast(m_Collider.bounds.center + m_Collider.bounds.extents.y * Vector3.right * MovementDirection, Vector3.right * MovementDirection, 0.2f, GroundMask);
        if (Hit)
        {
            MovementDirection *= -1;
        }

        Vector2 Velocity = m_Rigidibody.velocity;
        Velocity.x = MovementDirection * MovementSpeedInUnitsPerSecond;
        m_Rigidibody.velocity = Velocity;
    }

    public void Die(bool bWithBounce = false)
    {
        m_Alive = false;

        if (bWithBounce)
        {
            StartCoroutine(DieDelayed());

            Vector2 Velocity = DeathBounceVelocityInUnitsPerSecond;
            Velocity.x = Mathf.Sign(m_Rigidibody.velocity.x) * Mathf.Abs(Velocity.x);
            m_Rigidibody.velocity = Velocity;
            m_Collider.enabled = false;
            transform.localScale = new Vector3(1, -1, 1);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator DieDelayed()
    {
        yield return new WaitForSeconds(DeathDelayInSeconds);

        Die(false);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        CharacterController2DMario Character = collision.collider.GetComponent<CharacterController2DMario>();
        if (Character)
        {
            float Threshold = m_Collider.bounds.center.y + m_Collider.bounds.extents.y * 0.75f;

            foreach (ContactPoint2D Contact in collision.contacts)
            {
                if (Contact.point.y > Threshold)
                {
                    Vector2 TmpVelocity = collision.rigidbody.velocity;
                    TmpVelocity.y = PlayerBounceSpeedInUnitsPerSecond;
                    collision.rigidbody.velocity = TmpVelocity;
                    Die(false);
                    return;
                }
            }

            Character.Kill();
        }
    }
}
