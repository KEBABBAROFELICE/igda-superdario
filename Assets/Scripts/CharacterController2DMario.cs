using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class CharacterController2DMario : MonoBehaviour
{

    public float speed = 8f;
    public float jumpingPower = 16f;
    public float JumpGravityScale;
    public float FallGravityScale;
    public float JumpCooldownInSeconds;

    public float KillBouncePower;
    public float KillBounceGravityScale;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    private Collider2D m_Collider;
    private bool m_JumpRequested;
    private float m_JumpTimerInSeconds;
    private bool isFacingRight = true;

    private float horizontal;

    private void Awake()
    {
        TryGetComponent(out m_Collider);
    }

    // Start is called before the first frame update
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");

        if (!m_JumpRequested && Input.GetKeyDown(KeyCode.Space))
        {
            m_JumpRequested = true;
        }

        if (m_JumpTimerInSeconds > 0)
        {
            m_JumpTimerInSeconds = Mathf.Max(0, m_JumpTimerInSeconds - Time.deltaTime);
        }

        Flip();
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        Vector3 Velocity = rb.velocity;

        if (m_JumpRequested)
        {
            m_JumpRequested = false;

            if (IsGrounded() && m_JumpTimerInSeconds == 0)
            {
                Velocity.y = jumpingPower;
                m_JumpTimerInSeconds = JumpCooldownInSeconds;
            }
        }

        Velocity.x = speed * horizontal;

        rb.gravityScale = Velocity.y > 0 ? JumpGravityScale : FallGravityScale;

        rb.velocity = Velocity;

        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;

        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.GetType() == typeof(TilemapCollider2D))
        {
            Tilemap HitTilemap = other.collider.GetComponent<Tilemap>();
            foreach(ContactPoint2D Contact in other.contacts)
            {
                if (Contact.point.y >= m_Collider.bounds.center.y + m_Collider.bounds.extents.y)
                {
                    Vector3Int Cell = HitTilemap.WorldToCell(Contact.point + 0.2f * Vector2.up);

                    HittableTile HitTile = HitTilemap.GetTile<HittableTile>(Cell);
                    if (HitTile)
                    {
                        HitTile.ReceiveHit(Cell);
                        return;
                    }
                }
            }
        }
    }

    public void Kill()
    {
        m_Collider.enabled = false;
        rb.isKinematic = true;
        rb.simulated = false;

        StartCoroutine(KillBounce());

        IEnumerator KillBounce()
        {
            yield return new WaitForSeconds(0.5f);
            rb.simulated = true;
            rb.isKinematic = false;

            Vector3 vel = rb.velocity;
            vel.y = KillBouncePower;

            rb.velocity = vel;
            rb.gravityScale = KillBounceGravityScale;

            yield return new WaitForSeconds(2.5f);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
