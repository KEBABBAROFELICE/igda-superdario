using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryPole : MonoBehaviour
{
    public GameObject BravoCanvas;

    private void Start()
    {
        BravoCanvas.gameObject.SetActive(false);
        enabled = false;
    }

    private void Update()
    {
        if (BravoCanvas.activeSelf && Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CharacterController2DMario Character = collision.collider.GetComponent<CharacterController2DMario>();
        if (Character)
        {
            BravoCanvas.SetActive(true);
            enabled = true;
            Character.enabled = false;
            Rigidbody2D rb = Character.GetComponent<Rigidbody2D>();
            if (rb)
            {
                rb.velocity = Vector3.zero;
                rb.simulated = false;
            }

            collision.collider.enabled = false;
        }
    }
}
