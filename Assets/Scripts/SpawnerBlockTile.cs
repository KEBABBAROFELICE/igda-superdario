using UnityEngine;

[CreateAssetMenu(menuName = "Tiles/Spawner Block Tile")]
public class SpawnerBlockTile : HittableTile
{
    public GameObject PrefabToSpawn = null;
    public int NumHits = 1;
    public bool DefaultAnimation = true;

    public override void ReceiveHit(Vector3Int Coords)
    {
        base.ReceiveHit(Coords);

        BlockManager.Instance.SpawnObjectFromBlock(PrefabToSpawn, Coords, DefaultAnimation);
        BlockManager.Instance.RegisterBlockHit(Coords, NumHits);
    }
}
