using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HittableTile : Tile
{
    public Sprite PreviewSprite;
    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);

        if (!Application.isPlaying && PreviewSprite)
        {
            tileData.sprite = PreviewSprite;
        }
    }

    public virtual void ReceiveHit(Vector3Int Coords)
    {
        BlockManager.Instance.BumpBlock(Coords);
    }
}
