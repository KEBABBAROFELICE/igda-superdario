using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Tiles/Generic Tile")]
public class GenericTile : Tile {}
