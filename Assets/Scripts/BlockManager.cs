using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BlockManager : MonoBehaviour
{
    struct TileBump
    {
        public Vector3Int Cell;
        public float Time;

        public void Reset()
        {
            Cell = Vector3Int.zero;
            Time = -1;
        }
    }

    struct ObjectSpawn
    {
        public Transform ObjectTransform;
        public float FromY;
        public float ToY;
        public float Time;

        public void Reset()
        {
            ObjectTransform = null;
            FromY = 0.0f;
            ToY = 0.0f;
            Time = -1;
        }
    }

    public static BlockManager Instance => m_Instance;
    private static BlockManager m_Instance;

    [Header("Bump Animation")]
    public float BumpDistance;
    public float BumpDuration;

    [Header("Prefab Spawn Animation")]
    public float SpawnDuration;

    [Header("References")]
    public Tilemap TargetTilemap;
    public Tile ExhaustedTile;

    private TileBump[] m_TileBumps = new TileBump[20];
    private ObjectSpawn[] m_ObjectSpawns = new ObjectSpawn[20];
    private Dictionary<Vector3Int, int> m_BlockHits = new Dictionary<Vector3Int, int>();

    private void Awake()
    {
        if (m_Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        for (int i = 0; i < m_TileBumps.Length; ++i)
        {
            m_TileBumps[i].Reset();
        }

        for (int i = 0; i < m_ObjectSpawns.Length; ++i)
        {
            m_ObjectSpawns[i].Reset();
        }
    }

    private void Update()
    {
        UpdateBumps();
        UpdateObjectSpawns();
    }

    private void UpdateBumps()
    {
        for (int i = 0; i < m_TileBumps.Length; ++i)
        {
            if (m_TileBumps[i].Time < 0)
                continue;

            float NewTime = Mathf.Min(m_TileBumps[i].Time + Time.deltaTime, BumpDuration);
            Vector3 Translation = Vector3.zero;
            float x = 2 * NewTime / BumpDuration;
            Translation.Set(0, BumpDistance * x * (2 - x), 0);

            TargetTilemap.SetTransformMatrix(m_TileBumps[i].Cell, Matrix4x4.Translate(Translation));

            if (NewTime < BumpDuration)
            {
                m_TileBumps[i].Time = NewTime;
            }
            else
            {
                m_TileBumps[i].Reset();
            }
        }
    }

    private void UpdateObjectSpawns()
    {
        for (int i = 0; i < m_ObjectSpawns.Length; ++i)
        {
            if (m_ObjectSpawns[i].Time < 0)
                continue;

            float NewTime = Mathf.Min(m_ObjectSpawns[i].Time + Time.deltaTime, SpawnDuration);

            Vector3 ObjectLocation = m_ObjectSpawns[i].ObjectTransform.position;
            ObjectLocation.y = Mathf.Lerp(m_ObjectSpawns[i].FromY, m_ObjectSpawns[i].ToY, Mathf.Clamp(NewTime / SpawnDuration, 0, 1));

            m_ObjectSpawns[i].ObjectTransform.position = ObjectLocation;

            if (NewTime < SpawnDuration)
            {
                m_ObjectSpawns[i].Time = NewTime;
            }
            else
            {
                Collider2D Collider = m_ObjectSpawns[i].ObjectTransform.GetComponent<Collider2D>();
                if (Collider)
                {
                    Collider.enabled = true;
                }

                Rigidbody2D Rigidbody = m_ObjectSpawns[i].ObjectTransform.GetComponent<Rigidbody2D>();
                if (Rigidbody)
                {
                    Rigidbody.simulated = true;
                }

                m_ObjectSpawns[i].Reset();
            }
        }
    }

    public void BumpBlock(Vector3Int Cell)
    {
        for (int i = 0; i < m_TileBumps.Length; ++i)
        {
            if (m_TileBumps[i].Time < 0)
            {
                m_TileBumps[i].Cell = Cell;
                m_TileBumps[i].Time = 0.0f;

                Collider2D[] HitColliders = Physics2D.OverlapBoxAll(TargetTilemap.GetCellCenterWorld(Cell) + Vector3.up * TargetTilemap.cellSize.y / 2, new Vector2(TargetTilemap.cellSize.x, TargetTilemap.cellSize.y * 0.3f), 0.0f);

                foreach (Collider2D Coll in HitColliders)
                {
                    GoombaController Goomba = Coll.GetComponent<GoombaController>();
                    if (Goomba)
                    {
                        Goomba.Die(true);
                    }
                }
                return;
            }
        }
    }

    public void SpawnObjectFromBlock(GameObject Prefab, Vector3Int Cell, bool bDefaultAnimation = true)
    {
        Vector3 SpawnLocation = TargetTilemap.GetCellCenterWorld(Cell);
        Transform ObjectTransform = Instantiate(Prefab, SpawnLocation, Quaternion.identity, null).transform;

        if (bDefaultAnimation)
        {
            for (int i = 0; i < m_ObjectSpawns.Length; ++i)
            {
                if (m_ObjectSpawns[i].Time < 0)
                {
                    Vector3Int TargetCell = Cell;
                    TargetCell.y++;

                    m_ObjectSpawns[i].ObjectTransform = ObjectTransform;
                    m_ObjectSpawns[i].FromY = SpawnLocation.y;
                    m_ObjectSpawns[i].ToY = TargetTilemap.GetCellCenterWorld(TargetCell).y;
                    m_ObjectSpawns[i].Time = 0.0f;

                    Collider2D Collider = ObjectTransform.GetComponent<Collider2D>();
                    if (Collider)
                    {
                        Collider.enabled = false;
                    }

                    Rigidbody2D Rigidbody = m_ObjectSpawns[i].ObjectTransform.GetComponent<Rigidbody2D>();
                    if (Rigidbody)
                    {
                        Rigidbody.simulated = false;
                    }

                    return;
                }
            }
        }
    }

    public void ExhaustBlock(Vector3Int Cell)
    {
        TargetTilemap.SetTile(Cell, ExhaustedTile);
    }

    public void RegisterBlockHit(Vector3Int Coords, int MaxHits)
    {
        BumpBlock(Coords);

        if (!m_BlockHits.ContainsKey(Coords))
        {
            m_BlockHits.Add(Coords, 0);
        }

        int Num = m_BlockHits[Coords] + 1;

        if (Num >= MaxHits)
        {
            ExhaustBlock(Coords);
            m_BlockHits.Remove(Coords);
        }
        else
        {
            m_BlockHits[Coords] = Num;
        }
    }
}
