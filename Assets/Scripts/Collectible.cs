using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public int Value;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CharacterController2DMario Controller = collision.GetComponentInParent<CharacterController2DMario>();
        if (Controller)
        {
            //Controller.IncrementScore(Value);
            Destroy(gameObject);
        }
    }
}


